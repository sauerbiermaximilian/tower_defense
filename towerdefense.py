import pygame
import sys
from constant import *
from functions import *
from ingame_menu import *
from player import p
from timer import *
from wave import *

pygame.init()
pygame.display.set_caption('Tower Defense')

start_screen()

all_sprites.add(p)

while True:
    if game_state[1] == 0:
        game_over()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            timer.cancel()
            exit_game()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                timer.cancel()
                exit_game()
            elif event.key == pygame.K_p:
                timer.time_break_function()
                pause_game()
                timer.time_break_function()



        # ---upgrade button events---
        range_button.event_handler(event)
        damage_button.event_handler(event)
        atkspeed_button.event_handler(event)

    # Background
    screen.blit(bg_image, (0, 0))

    # Wall
    screen.blit(wall, (-150, -50))

    # Allies
    # screen.blit(ally1, (50, lane1))
    # screen.blit(ally2, (50, lane2))
    # screen.blit(ally3, (50, lane3))
    # screen.blit(ally4, (50, lane4))
    # screen.blit(ally5, (15, lane5))

    # upgrade menu
    upgrade_menu()

    # Waves
    wave_text_function()

    all_sprites.update()
    all_sprites.draw(screen)

    enemy_sprites.update()
    enemy_sprites.draw(screen)
    pygame.display.update()
    clock.tick(fps)

    hits = pygame.sprite.groupcollide(enemy_sprites, arrows, True, True)
