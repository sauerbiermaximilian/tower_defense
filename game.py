# coins and life
game_state = [100, 100]


def check_gameover():
    if game_state[1] == 0:
        print('Game Over!')


def decrease_life(amount):
    x = game_state[1] - amount
    game_state[1] = x


def increase_gold(amount):
    x = game_state[0] + amount
    game_state[0] = x


def decrease_gold(amount):
    x = game_state[0] - amount
    game_state[0] = x


