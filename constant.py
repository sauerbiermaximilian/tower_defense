import pygame

# Constants

# Game
width = 1150
height = 800
fps = 60
screen = pygame.display.set_mode((width, height))
clock = pygame.time.Clock()
all_sprites = pygame.sprite.Group()
enemy_sprites = pygame.sprite.Group()
arrows = pygame.sprite.Group()
ally_sprites = pygame.sprite.Group()
distance_to_Base = 137
enemy_frequency = 3
wave_time = 45
wave_break_time = 15

# Colors
menu_color = (0, 170, 255)

# lanes
lane1 = 30 + (((height - 60) / 5) * 0)
lane2 = 30 + (((height - 60) / 5) * 1)
lane3 = 30 + (((height - 60) / 5) * 2)
lane4 = 30 + (((height - 60) / 5) * 3)
lane5 = 30 + (((height - 60) / 5) * 4)

# images
# background
bg_image = pygame.transform.scale(pygame.image.load("./assets/background2.jpg"), (1150, 800))

# enemies
mob_easy_one = pygame.transform.scale(pygame.image.load("./assets/mobs/easy_one.png"), (100, 100))
mob_easy_two = pygame.transform.scale(pygame.image.load("./assets/mobs/easy_two.png"), (150, 75))
mob_easy_three = pygame.transform.scale(pygame.image.load("./assets/mobs/easy_three.png"), (150, 75))
mob_medium_one = pygame.transform.scale(pygame.image.load("./assets/mobs/medium_one.png"), (120, 120))
mob_medium_two = pygame.transform.scale(pygame.image.load("./assets/mobs/medium_two.png"), (120, 120))
mob_medium_three = pygame.transform.scale(pygame.image.load("./assets/mobs/medium_three.png"), (180, 150))
mob_hard_one = pygame.transform.scale(pygame.image.load("./assets/mobs/hard_one.png"), (150, 150))
mob_hard_two = pygame.transform.scale(pygame.image.load("./assets/mobs/hard_two.png"), (210, 175))
mob_hard_three = pygame.transform.scale(pygame.image.load("./assets/mobs/hard_three.png"), (180, 180))
mob_boss = pygame.image.load("./assets/mobs/boss.png")

enemys = [mob_easy_one, mob_easy_two, mob_easy_three, mob_medium_one, mob_medium_two, mob_medium_three, mob_hard_one, mob_hard_two, mob_hard_three, mob_boss]

# wall
wall = pygame.transform.scale(pygame.image.load("./assets/wall/wall.png"), (480, 900))

# player
player = pygame.transform.scale(pygame.image.load("./assets/player/player.png"), (350, 175))

arrow = pygame.transform.scale(pygame.image.load("./assets/player/arrow.png"), (100, 50))

# ingame menu
menu_bar = pygame.transform.scale(pygame.image.load("./assets/ingame_menu/ingame_menu_bar.png"), (100, 800))
damage_icon = pygame.transform.scale(pygame.image.load("./assets/ingame_menu/damage_icon.png"), (50, 50))
range_icon = pygame.transform.scale(pygame.image.load("./assets/ingame_menu/range_icon.png"), (50, 50))
atkspeed_icon = pygame.transform.scale(pygame.image.load("./assets/ingame_menu/atkspeed_icon.png"), (50, 50))
