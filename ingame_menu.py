import sys
from functions import *
import pygame
from constant import atkspeed_icon, damage_icon, range_icon, menu_bar
from player import p
from game import *

# initializing font
pygame.font.init()
myfont = pygame.font.SysFont("Arial", 17)

# static button texts
range_text = myfont.render("Range", False, (0, 0, 0))
damage_text = myfont.render("Damage", False, (0, 0, 0))
atkspeed_text = myfont.render("Atkspeed", False, (0, 0, 0))


def cost(level):
    return level * 2


def upgrade_cost_text_color(level):
    if game_state[0] > cost(level):
        return 50, 205, 50  # green
    else:
        return 139, 0, 0  # red


class Button(object):

    def __init__(self, position, name, image):

        self.name = name
        self.image = image
        self._rect = pygame.Rect(position, (50, 50))

    def draw(self, screen):
        screen.blit(self.image, self._rect)

    def event_handler(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # is left button clicked
                if self._rect.collidepoint(event.pos):  # is mouse over button
                    if self.name == "range" and game_state[0] > cost(p.range):
                        decrease_gold(cost(p.range))
                        p.upgrade_range()
                    if self.name == "damage" and game_state[0] > cost(p.damage):
                        decrease_gold(cost(p.damage))
                        p.upgrade_damage()
                    if self.name == "atkspeed" and game_state[0] > cost(p.atkspeed):
                        decrease_gold(cost(p.damage))
                        p.upgrade_atkspeed()


# create buttons
range_button = Button((1075, 400), "range", range_icon)
damage_button = Button((1075, 600), "damage", damage_icon)
atkspeed_button = Button((1075, 200), "atkspeed", atkspeed_icon)


def upgrade_menu():

    life_text = myfont.render("Leben: " + str(game_state[1]) + "/100", False, (0, 170, 255))
    coins_text = myfont.render("Coins: " + str(game_state[0]), False, (0, 170, 255))
    screen.blit(coins_text, (600, 20))
    screen.blit(life_text, (800, 20))

    # ---render dynamic button texts---
    # level
    range_lvl = myfont.render("Level : " + str(p.range), False, (0, 0, 0))
    damage_lvl = myfont.render("Level :" + str(p.damage), False, (0, 0, 0))
    atkspeed_lvl = myfont.render("Level: " + str(p.atkspeed), False, (0, 0, 0))

    # cost
    range_cost_text = myfont.render(str(cost(p.range)) + " Coins", False, upgrade_cost_text_color(p.range))
    damage_cost_text = myfont.render(str(cost(p.damage)) + " Coins", False, upgrade_cost_text_color(p.damage))
    atkspeed_cost_text = myfont.render(str(cost(p.atkspeed)) + " Coins", False, upgrade_cost_text_color(p.atkspeed))

    # menubar
    screen.blit(menu_bar, (1050, 0))

    # attackspeed button and texts
    screen.blit(atkspeed_text, (1070, 170))
    atkspeed_button.draw(screen)
    screen.blit(atkspeed_lvl, (1075, 250))
    screen.blit(atkspeed_cost_text, (1075, 270))

    # range button and texts
    screen.blit(range_text, (1075, 370))
    range_button.draw(screen)
    screen.blit(range_lvl, (1075, 450))
    screen.blit(range_cost_text, (1075, 470))

    # damage button and texts
    screen.blit(damage_text, (1075, 570))
    damage_button.draw(screen)
    screen.blit(damage_lvl, (1075, 650))
    screen.blit(damage_cost_text, (1075, 670))
