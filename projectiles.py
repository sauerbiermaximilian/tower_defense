import pygame
from constant import *


class Projectile(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = arrow
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x
        self.speedx = 3

    def update(self):
        self.rect.x += self.speedx
        # if out of screen remove it
        if self.rect.centerx > width - 150:
            self.kill()
