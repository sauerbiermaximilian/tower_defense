import pygame
from constant import *
import random
import math
from game import decrease_life



class Enemy(pygame.sprite.Sprite):
    def __init__(self, type):
        pygame.sprite.Sprite.__init__(self)
        self.image = enemys[random_enemy(type)]
        self.rect = self.image.get_rect()
        self.rect.x = width - 160
        self.rect.y = y_coordinate()
        #Speed
        self.change_x = 1.5
        
        if type == 'easy':
            self.life = 25
            self.damage = 1
        elif type == 'medium':
            self.life = 50
            self.damage = 2
        elif type == 'hard':
            self.life = 100
            self.damage = 5
        elif type == 'boss':
            self.life = 200
            self.damage = 10

    def update(self):
        # Border Checking - If Enemys pass the Castle, they dissapear
        if self.rect.x > distance_to_Base:
            self.rect.x -= self.change_x
       
        # If Enemy hits the Border it will despawn
        elif self.rect.x <= distance_to_Base or self.life <= 0:
            decrease_life(self.damage)
            self.kill()  


def y_coordinate():
    # Number between 1 to 5 for 5 enemy lanes
    random_number = math.ceil(random.randint(1, 5))
    if random_number == 1:
        return lane1
    elif random_number == 2:
        return lane2
    elif random_number == 3:
        return lane3
    elif random_number == 4:
        return lane4
    elif random_number == 5:
        return lane5


# Returns a random number for enemy sprite spawn
def random_enemy(enemy_type):
    #for enemy in range(3):
    if enemy_type == 'easy':
        random_number = math.ceil(random.randint(1, 3))
        if random_number == 1:
            return 0
        elif random_number == 2:
            return 1
        elif random_number == 3:
            return 2
    elif enemy_type == 'medium':
        random_number = math.ceil(random.randint(4, 6))
        if random_number == 4:
            return 3
        elif random_number == 5:
            return 4
        elif random_number == 6:
            return 5
    elif enemy_type == 'hard':
        random_number = math.ceil(random.randint(7, 9))
        if random_number == 7:
            return 6
        elif random_number == 8:
            return 7
        elif random_number == 9:
            return 8
    elif enemy_type == 'boss':
        return 9