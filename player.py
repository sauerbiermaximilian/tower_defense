from constant import *
from projectiles import *
import pygame


class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = player
        self.rect = self.image.get_rect()
        self.rect.x = -80
        self.rect.y = lane3
        self.life = 100
        self.last_shot = pygame.time.get_ticks()
        self.shoot_delay = 250
        self.damage = 1
        self.atkspeed = 1
        self.range = 1

    def update(self):
        key = pygame.key.get_pressed()
        if key[pygame.K_UP]:
            if self.rect.y <= lane1:
                self.rect.y = lane1
            else:
                self.rect.y -= 30

        if key[pygame.K_DOWN]:
            if self.rect.y >= lane5:
                self.rect.y = lane5
            else:
                self.rect.y += 30

        if key[pygame.K_SPACE]:
            self.shoot()

    def upgrade_damage(self):
        self.damage += 1

    def upgrade_range(self):
        self.range += 1

    def upgrade_atkspeed(self):
        self.atkspeed += 1

    def shoot(self):
        now = pygame.time.get_ticks()
        if now - self.last_shot > self.shoot_delay:
            self.last_shot = now
            arrow_projectile = Projectile(self.rect.x + 150, self.rect.y + 70)
            all_sprites.add(arrow_projectile)
            arrows.add(arrow_projectile)


# create player
p = Player()
