from threading import Timer, Thread, Event
import threading
from enemy import *
from constant import *


class wave_timer():

   def __init__(self,t,hFunction):
      self.t = t
      self.hFunction = hFunction
      self.thread = Timer(self.t,self.handle_function)
      self.wave_break = True
      self.t_break = False
      self.wave = 1
      self.second_counter = 1

   def handle_function(self):
      self.hFunction()
      self.thread = Timer(self.t,self.handle_function)
      self.thread.start()

   # Starts the timer
   def start(self):
      self.thread.start()

   # Stops the timer
   def cancel(self):
      self.thread.cancel()

   # Toggles the enemy waves 
   def wave_break_function(self):
      if self.wave_break == False:
         self.wave_break = True
      else:
         self.wave_break = False
         
   # Toggles the game break 
   def time_break_function(self):
      if self.t_break == False:
         self.t_break = True
      else:
         self.t_break = False

   # Counts the seconds between waves and changes between waves and breaks
   def wave_function(self):
      # If True: Wave Break
      if self.wave_break == True and self.t_break == False:
         self.second_counter += 1

      # If False: Wave Start
      if self.wave_break == False and self.t_break == False:
         self.second_counter += 1
      
      # If change between break and wave
      if self.second_counter == wave_break_time and self.wave_break == True and self.t_break == False:
         self.second_counter = 0
         self.wave_break_function()

      # If change between wave and break
      if self.second_counter == wave_time and self.wave_break == False and self.t_break == False:
         self.second_counter = 0
         self.wave += 1
         self.wave_break_function()

      # Wave is more than 10
      if self.wave > 10:
         self.wave = 1

def create_wave():
   timer.wave_function()

   if timer.wave_break == False and timer.t_break == False and timer.second_counter % enemy_frequency == 0:
      # Add sprites to the wave
      if timer.wave == 1:
        enemy_sprites.add(Enemy('easy'))

      elif timer.wave == 2:
         enemy_sprites.add(Enemy('medium'))

      elif timer.wave == 3:
         enemy_sprites.add(Enemy('hard'))

      elif timer.wave == 4:
         enemy_sprites.add(Enemy('easy'), Enemy('medium'))

      elif timer.wave == 5:
         enemy_sprites.add(Enemy('medium'), Enemy('medium'))

      elif timer.wave == 6:
         enemy_sprites.add(Enemy('medium'), Enemy('hard'))

      elif timer.wave == 7:
         enemy_sprites.add(Enemy('hard'), Enemy('hard'))

      elif timer.wave == 8:
         enemy_sprites.add(Enemy('hard'), Enemy('hard'), Enemy('hard'))

      elif timer.wave == 9:
         enemy_sprites.add(Enemy('boss'), Enemy('boss'))

      elif timer.wave == 10:
         enemy_sprites.add(Enemy('boss'), Enemy('boss'), Enemy('boss'))

timer = wave_timer(1 , create_wave)
timer.start()