import pygame
from constant import *
from timer import *

font = pygame.font.SysFont("Arial", 17)


def wave_text_function():
    waves_text = font.render("Wave: " + str(timer.wave) + " (noch " + str(wave_time - timer.second_counter) + " Sekunden)", False, (0, 170, 255))
    waves_break_text = font.render("Die Wave startet in: " + str(wave_break_time - timer.second_counter), False, (0, 170, 255))

    if timer.wave_break == True:
        screen.blit(waves_break_text, (350, 20))

    elif timer.wave_break == False:
        screen.blit(waves_text, (350, 20))

