import sys
import pygame
from constant import *
from timer import *


# exit the game
def exit_game():
    pygame.quit()
    sys.exit()


def create_img(img, w, h):
    bg = pygame.image.load(img)
    rect = bg.get_rect(center=(w, h))
    screen.fill(menu_color)
    screen.blit(bg, rect)


# shows pause the game screen
def pause_game():
    create_img('./assets/pause_game.png', 560, 400)
    pygame.display.update()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                timer.cancel()
                exit_game()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    timer.cancel()
                    exit_game()
                return


# shows start screen
def start_screen():
    create_img('./assets/main_menu.png', 600, 400)
    pygame.display.update()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                timer.cancel()
                exit_game()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    timer.cancel()
                    exit_game()
                return


# shows gameover screen
def game_over():
    create_img('./assets/game_over.png', 560, 400)
    pygame.display.update()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                timer.cancel()
                exit_game()
            elif event.type == pygame.KEYDOWN:
                timer.cancel()
                exit_game()

